#!/bin/bash
# Get the IP address of the GitLab container

docker network inspect workfront_default | \
    jq '.[].Containers | .[] |
      select(.Name == "workfront_gitlab_1") | .IPv4Address' | \
    sed -e 's/"//g' -e 's/\/.*//'

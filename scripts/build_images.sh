#!/bin/bash
# Build custom Docker images

cd filebeat || exit
docker build -t workfront/filebeat:latest .

cd ../mailer || exit
docker build -t workfront/mailer:latest .

cd ../runner || exit
docker build -t workfront/runner:latest .

#!/usr/bin/env python
import os
import pika
import smtplib
from email.mime.text import MIMEText

USERNAME = 'devops.display@sparefoot.com'
PASSWORD = os.environ['GMAIL_PASSWORD']
TO = 'dustinbphipps@gmail.com'

params = pika.ConnectionParameters(host='rabbitmq')
connection = pika.BlockingConnection(params)
channel = connection.channel()

channel.queue_declare(queue='jobs')

def callback(ch, method, properties, body):
    msg = MIMEText('Job {} was completed.'.format(bytes.decode(body)))

    msg['To'] = TO
    msg['Subject'] = 'Job finished'

    server = smtplib.SMTP('smtp.gmail.com:587')

    server.ehlo()
    server.starttls()
    server.login(USERNAME, PASSWORD)
    server.sendmail('', [TO], msg.as_string())
    server.quit()

channel.basic_consume(callback,
                      queue='jobs',
                      no_ack=True)

print('Waiting for messages ...')

channel.start_consuming()

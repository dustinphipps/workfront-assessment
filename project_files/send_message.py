#!/usr/bin/env python
import sys
import pika
import logging

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.CRITICAL)

params = pika.ConnectionParameters(host='rabbitmq')
connection = pika.BlockingConnection(params)
channel = connection.channel()

channel.queue_declare(queue='jobs')

channel.basic_publish(exchange='',
                      routing_key='jobs',
                      body=sys.argv[1])

connection.close()
